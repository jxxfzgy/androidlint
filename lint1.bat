#!/bin/bash  
#  
  
echo "Run Android Lint, Please Wait..."  
  
#IS_RUN_ANDROID_LINT=$(lint .)  
  
echo   
echo "Run Android Lint Result:"  
  
ERROR_COUNT=0;  
WARNINGS_COUNT=0;  
  
if [[ $IS_RUN_ANDROID_LINT =~ ([0-9]{1,4})( errors)(\, )([0-9]{1,4})( warnings) ]];then  
ERROR_COUNT=${BASH_REMATCH[1]}  
WARNINGS_COUNT=${BASH_REMATCH[4]}  
  
echo "We Found "$ERROR_COUNT" Lint Errors And "$WARNINGS_COUNT" Lint Warnings."  
  
    if [ $ERROR_COUNT -gt 0 ];then  
        #have lint error  
        echo "Please Fix the "$ERROR_COUNT" Errors"  
        #exit 1  
    else  
        #no error, just warning  
        echo  
    fi  
else  
echo "No Android Lint issues found."  
fi  
echo "Run <span style="font-family: Arial, Helvetica, sans-serif;">Android </span>Lint: Yes"  
echo